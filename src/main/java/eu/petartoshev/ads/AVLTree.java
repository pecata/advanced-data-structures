package eu.petartoshev.ads;

public class AVLTree<E extends Comparable<E>> {

  static int getHeight(AVLTreeNode node) {
    if(node == null) return 0;
    return node.height;
  }

  private AVLTreeNode<E> root = null;

  public boolean add(E element) {
    root = insert(root, element);
    return true;
  }

  private AVLTreeNode<E> rightRotate(AVLTreeNode<E> node) {
//    System.out.print("[R " + node.data + "]\t");
    AVLTreeNode<E> left = node.left;
    node.left = left.right;
    left.right = node;
    node.updateHeight();
    left.updateHeight();
    return left;
  }
  private AVLTreeNode<E> leftRotate(AVLTreeNode<E> node) {
//    System.out.print("[L " + node.data + "]\t");
    AVLTreeNode<E> right = node.right;
    node.right = right.left;
    right.left = node;
    node.updateHeight();
    right.updateHeight();
    return right;
  }


  private AVLTreeNode<E> insert(AVLTreeNode<E> node, E element) {
    if (node == null) {
      return new AVLTreeNode<E>(element);
    } else if (element.compareTo(node.data) < 0) {
      node.left = insert(node.left, element);
    } else if (element.compareTo(node.data) > 0) {
      node.right = insert(node.right, element);
    } else {
      return node;
    }

    node.updateHeight();
    int balanced = getHeight(node.left) - getHeight(node.right);
    // Left * case
    if(balanced > 1){
      // Left Right case
      if(element.compareTo(node.left.data) > 0) node.left = leftRotate(node.left);
      return rightRotate(node);
    }

    // Right * case
    if(balanced < -1) {
      // Right Left case
      if (element.compareTo(node.right.data) < 0) node.right = rightRotate(node.right);
      return leftRotate(node);
    }

    return node;
  }

  public void print() {
    print(root, "");
    System.out.println();
  }

  private void print(AVLTreeNode<E> node, final String prefix) {
    if(node == null) return;
    System.out.print(prefix + node.data + "  ");
    print(node.left, prefix+"L");
    print(node.right, prefix+"R");
  }

  private static class AVLTreeNode<E> {

    final E data;
    int height = 1;
    AVLTreeNode<E> left = null;
    AVLTreeNode<E> right = null;

    AVLTreeNode(E data) {
      this.data = data;
    }

    void updateHeight() {
      height = Math.max(AVLTree.getHeight(left), AVLTree.getHeight(right)) + 1;
    }
  }

  public static class Main {
    public static void main(String[] args) {
      AVLTree<Integer> tree = new AVLTree<Integer>();
      tree.add(6);
      tree.add(8);
      tree.add(7);
      tree.add(5);
      tree.add(4);
      tree.add(3);
      tree.add(10);
      tree.add(11);
      tree.add(12);
      tree.print();
    }
  }

}
